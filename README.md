# Wumpus World
## Description
The Wumpus world is a simple maze-like game where the player agent has to take decisions based on conflicting information.

## Implementation
* Implemented artifical agent that takes necessary actions based on environment.

## Execution
* Execute WumpusWorld.java to observe the results.

### Note
This was an university project. An unimplmented project without the logic was given.
I implemented only the logic which would be in MyAgent.java. Use NetBeans IDE.