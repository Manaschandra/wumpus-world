package wumpusworld;



/**
 * Contans starting code for creating your own Wumpus World agent.
 * Currently the agent only make a random decision each turn.
 * 
 * @author Manas
 */
public class MyAgent implements Agent
{
    private World w;
       
    /**
     * Creates a new instance of your solver agent.
     * 
     * @param world Current world state 
     */
    public MyAgent(World world)
    {
        w = world;
    }
    
    /**
     * Asks your solver agent to execute an action.
     */
    public void doAction()
    {    
        

        //Location of the player
        int cX = w.getPlayerX();
        int cY = w.getPlayerY();
        
        
        
        //Basic action:
        //Grab Gold if we can.
        if (w.hasGlitter(cX, cY))
        {
            w.doAction(World.A_GRAB);
            return;
        }
        
        //Basic action:
        //We are in a pit. Climb up.
        if (w.isInPit())
        {
            w.doAction(World.A_CLIMB);
            return;
        }
         
        
        //Test the environment
        if (w.hasStench(cX, cY))
        {
            System.out.println("I am in a Stench");
            
            //checks topLeft,topRight,bottomLeft,bottomRight to determine the position of wumpus and shoots
            if((w.hasStench(cX+1,cY+1) && w.isValidPosition(cX+1, cY+1))) 
                {   
                    if(w.isUnknown(cX, cY+1)){
                       shootAt(World.DIR_UP);
                            goTO(World.DIR_UP);
                    }
                    else
                    {
                        shootAt(World.DIR_RIGHT);
                           goTO(World.DIR_RIGHT);
                    }
                }
            else if  (w.hasStench(cX+1, cY-1)&& w.isValidPosition(cX+1, cY-1)) 
            {
                if(w.isUnknown(cX+1, cY)){
                       shootAt(World.DIR_RIGHT);
                           goTO(World.DIR_RIGHT);
                    }
                    else
                    {
                        shootAt(World.DIR_DOWN);
                            goTO(World.DIR_DOWN);
                    }
            }
            else if((w.hasStench(cX-1,cY+1) && w.isValidPosition(cX-1, cY+1)))
            {    if(w.isUnknown(cX, cY+1)){
                       shootAt(World.DIR_UP);
                            goTO(World.DIR_UP);
                    }
                    else 
                    {
                        shootAt(World.DIR_LEFT);
                            goTO(World.DIR_LEFT);
                    }
            }
            else if  (w.hasStench(cX-1, cY-1)&& w.isValidPosition(cX-1, cY-1))
             {
                        if(w.isUnknown(cX-1, cY)){
                            if(w.hasArrow()){
                            shootAt(World.DIR_LEFT);
                            goTO(World.DIR_LEFT);}
                            else
                                 w.doAction(World.A_MOVE);
                         }
                          else 
                        {
                             if(w.hasArrow()){
                                shootAt(World.DIR_DOWN);
                                 goTO(World.DIR_DOWN);}
                             else
                                 w.doAction(World.A_MOVE);
                        }
             }

            else 
            {   //checks if stench is at first position i.e at (1,1)
                if((w.isUnknown(cX, cY+1) || !w.isValidPosition(cX, cY+1)) && (w.isUnknown(cX, cY-1) || !w.isValidPosition(cX, cY-1)) && (w.isUnknown(cX+1, cY) || !w.isValidPosition(cX+1, cY) ) && ((w.isUnknown(cX-1, cY) )|| !w.isValidPosition(cX-1, cY)) )
                { 
                    shootAt(World.DIR_RIGHT);
                    w.doAction(World.A_MOVE);
                }
                else  gotoExplored(cX, cY);
              
            }
        }
        else if (w.hasBreeze(cX, cY))
        {
            System.out.println("I am in a Breeze");
            gotoUnexplored(cX, cY);
       }
       else if (w.hasPit(cX, cY))
        {
            System.out.println("I am in a Pit");
            gotoUnexplored(cX, cY);
        }
   
        else if(w.isVisited(cX, cY))
        {
            gotoUnexplored(cX, cY);
        }
        
      
        if (w.getDirection() == World.DIR_RIGHT)
        {
            System.out.println("I am facing Right");
        }
        if (w.getDirection() == World.DIR_LEFT)
        {
            System.out.println("I am facing Left");
        }
        if (w.getDirection() == World.DIR_UP)
        {
            System.out.println("I am facing Up");
        }
        if (w.getDirection() == World.DIR_DOWN)
        {
            System.out.println("I am facing Down");
        }
        
      
}
    
    
    public void gotoExplored(int cX,int cY) //Move to explored states
    {
        if(w.isVisited(cX, cY+1) && w.isValidPosition(cX, cY+1))
            {
                
                goTO(World.DIR_UP);
            }
            else if(w.isVisited(cX+1, cY) && w.isValidPosition(cX+1, cY))
            {
                goTO(World.DIR_RIGHT);
            }
            else if(w.isVisited(cX-1, cY) && w.isValidPosition(cX-1, cY))
                    {
                        goTO(World.DIR_LEFT);
                    }
            else if(w.isVisited(cX, cY-1) && w.isValidPosition(cX, cY-1))
            {
                goTO(World.DIR_DOWN);
            }
            else rnd();
    }
 
    public void gotoUnexplored(int cX,int cY)// Move to unexplored states
    {
        if(w.isUnknown(cX, cY+1) && w.isValidPosition(cX, cY+1))
            {
                
                goTO(World.DIR_UP);
            }
            else if(w.isUnknown(cX+1, cY) && w.isValidPosition(cX+1, cY))
            {
                goTO(World.DIR_RIGHT);
            }
            else if(w.isUnknown(cX-1, cY) && w.isValidPosition(cX-1, cY))
                    {
                        goTO(World.DIR_LEFT);
                    }
            else if(w.isUnknown(cX, cY-1) && w.isValidPosition(cX, cY-1))
            {
                goTO(World.DIR_DOWN);
            }
            else rnd();
    } 
   public void goTO(int WantedDirection) // Move towards given direction
   {
       while(w.getDirection() != WantedDirection)
       {
        w.doAction(World.A_TURN_LEFT);
       }
          w.doAction(World.A_MOVE);
       
   }
      public void shootAt(int WantedDirection) //shoots the wumpus in the direction 
      {
          while(w.getDirection() != WantedDirection)
       {
        w.doAction(World.A_TURN_LEFT);
       }
          w.doAction(World.A_SHOOT);
      }
public void rnd() // random moves
{
    int rnd = (int)(Math.random() * 5);
        if (rnd == 0) 
        {
            w.doAction(World.A_TURN_LEFT); w.doAction(World.A_MOVE);
            return;
        }
        if (rnd == 1)
        {
            w.doAction(World.A_TURN_RIGHT); w.doAction(World.A_MOVE);
            return;
        }
        if (rnd >= 2)
        {
            w.doAction(World.A_MOVE);
            return;
        }
}
 
  
}
